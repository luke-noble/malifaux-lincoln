<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Profile;
use App\Result;

class ResultController extends Controller
{

    public function index()
    {
        $profiles = Profile::where('user_id', '!=', auth()->id())
            ->get();
    	return view('result', compact('profiles'));
    }
    public function show()
    {
    	$results = Result::latest()
            ->get()
            ->groupBy(function($item)
            {
                return $item->created_at->format('d-M-y');
            });
    	return view('showresults', compact('results'));
    }

    public function store()
    {
    	$this->validate(request(), [
            'opponent' => 'required',
            'score1' => 'required',
            'score2' => 'required'
            ]);
        $player = Profile::where('user_id', auth()->id())->first();
    	$opponent = Profile::where('username', request('opponent'))->first();
        $winner = Result::calcWinner(request('score1'), request('score2'));
        $eloChange = Result::scoreChange($player['elo'], $opponent['elo'], 30, $winner);
        $player->elo = $player['elo'] + $eloChange;
        $player->save();
        $opponent->elo = $opponent['elo'] - $eloChange;
        $opponent->save();

        Result::create([
            'score1' => request('score1'),
            'score2' => request('score2'),
            'opponent_id' => $opponent['user_id'],
            'player_id' => auth()->id()
            ]);
    	return redirect('/home');
    }
}
