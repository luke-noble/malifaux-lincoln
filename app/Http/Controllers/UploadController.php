<?php

namespace App\Http\Controllers;

use App\AvatarPicture;
use App\Profile;
use Illuminate\Http\Request;
use App\Http\Requests\UploadRequest;

class UploadController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function uploadForm()
    {
        return view('upload_form');
    }
 
    public function uploadSubmit(UploadRequest $request)
    {
    	$profile = Profile::where('user_id', auth()->id())->first();
        $filename = $request->avatar->store('public/avatars');
        AvatarPicture::create([
                'profile_id' => $profile->id,
                'filename' => $filename
        ]);

        return back();
    }
}
