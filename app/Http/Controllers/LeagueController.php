<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\League;

class LeagueController extends Controller
{
    public function __construct()
    {
        $this->middleware('role:admin');
    }

    public function index()
    {
    	return view('league.createleague');
    }

    public function store()
    {
    	$this->validate(request(), [
            'name' => 'required|unique:leagues',
            ]);

        League::create([
            'name' => request('name'),
            ]);
    	return redirect('/home');
    }

}
