<?php

namespace App\Http\Controllers;

use App\Profile;
use Illuminate\Http\Request;

class SetUsernameController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
    	return view('profiles.setusername');
    }

    public function store()
    {
    	$this->validate(request(), [
            'username' => 'required|unique:profiles|max:40',
            ]);

        Profile::create([
            'username' => request('username'),
            'elo' => 1000,
            'user_id' => auth()->id()
            ]);
    	return redirect('/home');
    }
}
