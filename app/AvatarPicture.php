<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AvatarPicture extends Model
{
    protected $fillable = ['profile_id', 'filename'];
 
    public function profile()
    {
        return $this->belongsTo('App\Profile');
    }
}
