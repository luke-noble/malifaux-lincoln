<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Result extends Model
{
    protected $fillable = [
        'player_id', 'opponent_id', 'score1', 'score2',
    ];
    
    public function opponent()
    {
    	return $this->belongsTo(Profile::class);
    }
    public function player()
    {
        return $this->belongsTo(Profile::class);
    }



    public static function scoreChange($elo1, $elo2, $k, $score1)
    {
        $t1 = pow(10, ($elo1/400));
        $t2 = pow(10, ($elo2/400));
        $e1 = $t1 / ($t1 + $t2);
        $scoreChange = round($k * ($score1 - $e1));
        return $scoreChange;
    }

    public static function calcWinner($score1, $score2)
    {
        if ($score1 > $score2)
        {
            return 1;
        }
        elseif ($score1 < $score2)
        {
            return 0;
        }
        else 
        {
            return 0.5;
        }
    }
}
