@extends ('layouts.app')

@section ('content')
	<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Submit Result</div>
                <div class="panel-body">
                    <form class="form-horizontal" role="form" method="POST" action="/submit-result">
                        {{ csrf_field() }}
                        <div class="form-group">
                        	<label for="opponent" class="col-md-4 control-label">Opponent</label>
                            <div class="col-md-6">
                        		<select name="opponent">
  									@foreach ($profiles as $profile)
  										<option value="{{ $profile->username }}">
  											{{ $profile->username }}
  										</option>
  									@endforeach
								</select>
							</div>
						</div>
						<div class="form-group{{ $errors->has('score1') ? ' has-error' : '' }}">
                            <label for="score1" class="col-md-4 control-label">Your score</label>

                            <div class="col-md-6">
                                <input id="score1" type="text" class="form-control" name="score1" value="{{ old('score1') }}" required>

                                @if ($errors->has('score1'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('score1') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('score2') ? ' has-error' : '' }}">
                            <label for="score2" class="col-md-4 control-label">Opponents score</label>

                            <div class="col-md-6">
                                <input id="score2" type="text" class="form-control" name="score2" value="{{ old('score2') }}" required>

                                @if ($errors->has('score2'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('score2') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-8 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Submit Result
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection