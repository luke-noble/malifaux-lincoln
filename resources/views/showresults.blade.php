@extends ('layouts.app')

@section ('content')
	<div class="container">
    	<div class="row">
        	<div class="col-md-8 col-md-offset-2">
            	<div class="panel panel-default">
                	<div class="panel-heading">Results</div>
                	<div class="panel-body">
						@foreach ($results as $key => $val)
							<div>{{ $key }}</div>
							@for ($i = 0; $i < sizeof($val) ; $i++) 
								<div>{{ $val[$i]->player->username }} {{ $val[$i]->score1}} : {{ $val[$i]->score2 }} {{ $val[$i]->opponent->username }}</div>
							@endfor
							<br>
						@endforeach
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection