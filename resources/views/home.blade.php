@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">League Table</div>

                <div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-striped">
                        <tr>   
                            <th>Position</th>
                            <th>Username</th>
                            <th>Elo Ranking</th>
                        </tr>
                        <?php $i = 1?>
                            @foreach ($profiles as $profile)
                                <tr>
                                    <td>
                                        <?php echo $i; $i++; ?> 
                                    </td>
                                    <td>
                                        {{ $profile->username }}
                                    </td>
                                    <td>
                                        {{ $profile->elo }}
                                    </td>
                                </tr>
                            @endforeach
                    </table>
                </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
