@extends ('layouts.app')

@section ('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Upload Avatar</div>
                    <div class="panel-body">
                        @if (count($errors) > 0)
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        @endif
                        <form action="/upload" method="post" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            Avatar:
                            <br />
                            <input type="file" name="avatar" />
                            <br /><br />
                            <input type="submit" value="Upload" class="btn btn-primary" />
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection