<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index');

Auth::routes();

Route::get('/home', 'HomeController@index');
Route::get('/submit-result', 'ResultController@index')->middleware('auth');
Route::post('/submit-result', 'ResultController@store')->middleware('auth');

Route::get('set-username', 'SetUsernameController@index');
Route::post('set-username', 'SetUsernameController@store');

Route::get('/results', 'ResultController@show');

Route::get('/create-league', 'LeagueController@index');
Route::post('/create-league', 'LeagueController@store');
 
Route::get('/upload', 'UploadController@uploadForm');
Route::post('/upload', 'UploadController@uploadSubmit');

