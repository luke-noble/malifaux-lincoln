<?php

namespace Tests\Unit;

use Tests\TestCase;
use App\User;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ExampleTest extends TestCase
{
	use DatabaseTransactions;
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testBaseRoute()
    {
        $response = $this->get('/');

        $response->assertStatus(200);
    }

    public function testHomeRoute()
    {
        $response = $this->get('/home');

        $response->assertStatus(200);
    }

    public function testSubmitResultRoute()
    {
    	$user = factory(User::class)->create();

        $response = $this->actingAs($user)
                         ->get('/submit-result');

        $response->assertStatus(200);
    }

    public function testResultsRoute()
    {
        $user = factory(User::class)->create();

        $response = $this->actingAs($user)
                         ->get('/results');

        $response->assertStatus(200);
    }

    public function testUsernameRoute()
    {
        $user = factory(User::class)->create();

        $response = $this->actingAs($user)
                         ->get('/set-username');

        $response->assertStatus(200);
    }

    public function testScoreChange()
    {
    	$score = \App\Result::scoreChange(1000, 1000, 30, 1);
    	$this->assertTrue($score == 15);
    }

    public function testCalcWinner()
    {
    	$score1 = \App\Result::calcWinner(5,4);
    	$score2 = \App\Result::calcWinner(5,5);
    	$score3 = \App\Result::calcWinner(4,5);
    	$this->assertTrue($score1 == 1 && $score2 == 0.5 && $score3 == 0);
    }
}
