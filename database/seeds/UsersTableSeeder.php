<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'Luke Noble',
            'email' => 'luke@example.com',
            'password' => bcrypt('password'),
        ]);
        DB::table('users')->insert([
            'name' => 'Zoe Coultan',
            'email' => 'zoe@example.com',
            'password' => bcrypt('password'),
        ]);
        DB::table('users')->insert([
            'name' => 'John Smith',
            'email' => 'john@example.com',
            'password' => bcrypt('password'),
        ]);
        DB::table('users')->insert([
            'name' => 'Bob Marley',
            'email' => 'bob@example.com',
            'password' => bcrypt('password'),
        ]);
        DB::table('users')->insert([
            'name' => 'Jane Doe',
            'email' => 'jane@example.com',
            'password' => bcrypt('password'),
        ]);
    }
}
