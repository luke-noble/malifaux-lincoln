<?php

use Illuminate\Database\Seeder;

class ProfilesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('profiles')->insert([
            'username' => 'Luzocono',
            'elo' => 1325,
            'user_id' => 1,
        ]);
        DB::table('profiles')->insert([
            'username' => 'HansZolo',
            'elo' => 931,
            'user_id' => 2,
        ]);
        DB::table('profiles')->insert([
            'username' => 'Zaikan',
            'elo' => 709,
            'user_id' => 3,
        ]);
        DB::table('profiles')->insert([
            'username' => 'Jemain',
            'elo' => 1043,
            'user_id' => 4,
        ]);
        DB::table('profiles')->insert([
            'username' => 'Mavorick',
            'elo' => 1002,
            'user_id' => 5,
        ]);
    }
}
